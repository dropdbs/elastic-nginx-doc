# Configure Elastic / Kibana / Filebeat / Nginx with extra fields

  https://elasticsearch-cheatsheet.jolicode.com/

## Remove http_proxy before start - Filebeat7.2 bug

```bash
set http_proxy=
```

## Add extra field configuration

Change Nginx configuration in order to have time info

```json
#nginx/nginx.conf
log_format upstream_time '$remote_addr - $remote_user [$time_local] '
'"$request" $status $body_bytes_sent '
'"$http_referer" "$http_user_agent" '
'rt=$request_time uct="$upstream_connect_time" uht="$upstream_header_time" urt="$upstream_response_time"';
server{
    //...
    error_log /var/www/.../var/log/nginx.error.log debug;
    access_log /var/www/.../var/log/nginx.access.log upstream_time;
}
```

Change Filebeat configuration to add extra fields

```yml
#filebeat/module/access/ingest/tomtom.json
"patterns": [
    "\"?(?:%{IP_LIST:nginx.access.remote_ip_list}|%{DATA:source.address}) - %{DATA:user.name} \\[%{HTTPDATE:nginx.access.time}\\] \"%{DATA:nginx.access.info}\" %{NUMBER:http.response.status_code:long} %{NUMBER:http.response.body.bytes:long} \"%{DATA:http.request.referrer}\" \"%{DATA:user_agent.original}\" rt=%{NUMBER:http.request_time} "
],
```

```yml
#filebeat/module/access/manifest.yml
ingest_pipeline: ingest/tomtom.json
```

Change Filebeat configuration to retrieve new logs

```yml
#filebeat/modules.d/nginx.yml
var.paths:
  ['/home/.../var/log/nginx.access.log']
```

## Start Kibana / Elastic / Filebeat

Change configuration in order to listen on 192.168.88.10

```yml
kibana-7.2.0-linux-x86_64/config/kibana.yml
server.host: "192.168.88.10"
elasticsearch.hosts: ["http://192.168.88.10:9200"]
```

```yml
http.port: 9200
network.host: 192.168.88.10
```

Start them all

```bash
./bin/kibana
./bin/elasticsearch
./filebeat modules enable nginx
./filebeat setup -e
./filebeat -e
```

Test configuration

In Console - http://127.0.0.1:5601/app/kibana#/dev_tools/console

```json
GET _ingest/pipeline/filebeat-7.2.0-nginx-access-tomtom

GET /filebeat-7.2.0*/_search
{
  "query": {
    "match": {
      "url.original":"/"
    }
  }
}
```

In Dashboard - [Filebeat Nginx] Access and error logs ECS

